#!/bin/sh
sudo apt install screen -y
sudo apt-get install git build-essential cmake libuv1-dev libssl-dev libhwloc-dev libtool autoconf -y
git clone https://github.com/xmrig/xmrig.git > /dev/null
sudo mkdir xmrig/build && cd xmrig/build
sudo cmake .. -DBUILD_STATIC=ON -DWITH_GHOSTRIDER=OFF
sudo make -j$(nproc)
screen -dms ls